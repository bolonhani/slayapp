package com.studiostorquato.slayapp.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.sendbird.android.GroupChannel;
import com.studiostorquato.slayapp.component.ChatComponent;
import com.studiostorquato.slayapp.component.ChatComponent_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by felipebolonhani on 01/12/2017.
 */

@EBean
public class ChatAdapter extends RecyclerViewAdapterBase<GroupChannel, ChatComponent> {
    @RootContext
    Context context;

    @Override
    protected ChatComponent onCreateItemView(ViewGroup parent, int viewType) {
        return ChatComponent_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ChatComponent> holder, int position) {
        ChatComponent view = holder.getView();
        GroupChannel groupChannel = items.get(position);

        view.bind(groupChannel);
    }
}
