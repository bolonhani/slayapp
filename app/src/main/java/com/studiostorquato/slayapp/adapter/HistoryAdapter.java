package com.studiostorquato.slayapp.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.studiostorquato.slayapp.component.HistoryComponent;
import com.studiostorquato.slayapp.component.HistoryComponent_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.HashMap;

/**
 * Created by felipebolonhani on 07/12/2017.
 */
@EBean
public class HistoryAdapter extends RecyclerViewAdapterBase<Object, HistoryComponent> {
    @RootContext
    Context context;

    @Override
    protected HistoryComponent onCreateItemView(ViewGroup parent, int viewType) {
        return HistoryComponent_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<HistoryComponent> holder, int position) {
        HistoryComponent view = holder.getView();
        HashMap call = (HashMap) items.get(position);
        view.bind(call);
    }
}
