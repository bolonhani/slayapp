package com.studiostorquato.slayapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by felipebolonhani on 01/12/2017.
 */

public class ViewWrapper<V extends View> extends RecyclerView.ViewHolder {

    private V view;

    public ViewWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        view.setOnClickListener(listener);
    }
}
