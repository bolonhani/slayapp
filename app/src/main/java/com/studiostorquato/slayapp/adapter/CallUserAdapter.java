package com.studiostorquato.slayapp.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.studiostorquato.slayapp.component.CallUserComponent;
import com.studiostorquato.slayapp.component.CallUserComponent_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.HashMap;

/**
 * Created by felipebolonhani on 05/12/2017.
 */
@EBean
public class CallUserAdapter extends RecyclerViewAdapterBase<String, CallUserComponent> {
    @RootContext
    Context context;

    @Override
    protected CallUserComponent onCreateItemView(ViewGroup parent, int viewType) {
        return CallUserComponent_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<CallUserComponent> holder, int position) {
        CallUserComponent view = holder.getView();
        String user = items.get(position);
        view.bind(user);
    }
}