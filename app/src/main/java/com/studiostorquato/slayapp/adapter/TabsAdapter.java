package com.studiostorquato.slayapp.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.studiostorquato.slayapp.views.DialerFragment;
import com.studiostorquato.slayapp.views.DialerFragment_;
import com.studiostorquato.slayapp.views.HistoryFragment_;
import com.studiostorquato.slayapp.views.PhoneContactsFragment;
import com.studiostorquato.slayapp.views.PhoneContactsFragment_;

import java.util.ArrayList;

/**
 * Created by felipebolonhani on 07/12/2017.
 */
public class TabsAdapter extends FragmentPagerAdapter {
    Context context;

    public TabsAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(DialerFragment_.builder().build());
        fragments.add(PhoneContactsFragment_.builder().build());
        fragments.add(HistoryFragment_.builder().build());

        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String pageTitle = "";

        switch (position) {
            case 0:
                pageTitle = "Discar";
                break;
            case 1:
                pageTitle = "Contatos";
                break;
            case 2:
                pageTitle = "Histórico";
                break;
        }
        return pageTitle;
    }
}
