package com.studiostorquato.slayapp.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.sendbird.android.User;
import com.studiostorquato.slayapp.component.UserComponent;
import com.studiostorquato.slayapp.component.UserComponent_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by felipebolonhani on 01/12/2017.
 */
@EBean
public class UserAdapter extends RecyclerViewAdapterBase<User, UserComponent> {
    @RootContext
    Context context;

    @Override
    protected UserComponent onCreateItemView(ViewGroup parent, int viewType) {
        return UserComponent_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<UserComponent> holder, int position) {
        UserComponent view = holder.getView();
        User user = items.get(position);
        view.bind(user);
    }
}
