package com.studiostorquato.slayapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sendbird.android.BaseMessage;
import com.sendbird.android.UserMessage;
import com.studiostorquato.slayapp.component.TextMessageComponent;
import com.studiostorquato.slayapp.component.TextMessageComponent_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by felipebolonhani on 02/12/2017.
 */
@EBean
public class MessageAdapter extends RecyclerViewAdapterBase<BaseMessage, TextMessageComponent> {
    @RootContext
    Context context;

    @Override
    protected TextMessageComponent onCreateItemView(ViewGroup parent, int viewType) {
        return TextMessageComponent_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<TextMessageComponent> holder, int position) {
        TextMessageComponent view = holder.getView();
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        BaseMessage message = items.get(position);

        view.bind(message);
    }
}
