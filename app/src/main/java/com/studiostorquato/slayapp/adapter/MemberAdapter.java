package com.studiostorquato.slayapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sendbird.android.User;
import com.studiostorquato.slayapp.component.MemberComponent;
import com.studiostorquato.slayapp.component.MemberComponent_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by felipebolonhani on 09/12/2017.
 */
@EBean
public class MemberAdapter extends RecyclerViewAdapterBase<User, MemberComponent> {
    @RootContext
    Context context;

    @Override
    protected MemberComponent onCreateItemView(ViewGroup parent, int viewType) {
        return MemberComponent_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<MemberComponent> holder, int position) {
        MemberComponent view = holder.getView();
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        User user = items.get(position);
        view.bind(user);
    }
}
