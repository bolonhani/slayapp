package com.studiostorquato.slayapp.views;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.sinch.android.rtc.SinchClient;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;
import com.studiostorquato.slayapp.adapter.TabsAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FocusChange;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_dialer)
public class DialerActivity extends SlayAppActivity {
    @ViewById(R.id.toolbar2)
    Toolbar toolbar;

    @ViewById
    TabLayout tabLayout;

    @ViewById
    ViewPager pager;

    @AfterViews
    void afterViews() {
        createToolbar(toolbar);
        TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager(), this);
        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);

        String[] requiredPermissions = new String[]{
                Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.READ_CONTACTS
        };

        if (!hasPermissions(this, requiredPermissions)) {
            requestPermissions(this, requiredPermissions, 0);
        }
    }
}
