package com.studiostorquato.slayapp.views;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.sendbird.android.User;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;
import com.studiostorquato.slayapp.adapter.CallUserAdapter;
import com.studiostorquato.slayapp.sendBirdApi.UserService;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

@EActivity(R.layout.activity_dial_contacts)
public class DialContactsActivity extends SlayAppActivity {
    Context context;
    Retrofit retrofit;
    User currentUser;

    ArrayList<String> users = new ArrayList<String>();
    List<User> sendBirdUsers;

    @ViewById
    RecyclerView contacts;

    @Bean
    CallUserAdapter userAdapter;

    @AfterViews
    void afterViews() {
        context = getApplicationContext();
        final Activity _this = this;

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        contacts.setLayoutManager(linearLayoutManager);
        contacts.setAdapter(userAdapter);

        retrofit = Core.getRetrofit();
        UserService userService = retrofit.create(UserService.class);
        userService.listUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                sendBirdUsers = response.body();

                if (hasPermission(_this, Manifest.permission.READ_CONTACTS)) {
                    getPhoneNumbers();
                    userAdapter.setItems(users);
                } else {
                    requestPermission(_this, Manifest.permission.READ_CONTACTS, 1);
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPhoneNumbers();
            }
        }
    }

    void getPhoneNumbers() {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor contacts = getContentResolver().query(uri, projection, null, null, null);

        if (contacts != null) {
            int indexPhoneNumber = contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int indexName = contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            contacts.moveToFirst();

            do {
                String phoneNumber = contacts.getString(indexPhoneNumber).replaceAll("\\D+", "");
                String name = contacts.getString(indexName);
                users.add(name + " | " + phoneNumber);
                Collections.sort(users);
            } while (contacts.moveToNext());
        }
    }
}
