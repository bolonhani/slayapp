package com.studiostorquato.slayapp.views;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.sendbird.android.User;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;
import com.studiostorquato.slayapp.adapter.CallUserAdapter;
import com.studiostorquato.slayapp.sendBirdApi.UserService;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by felipebolonhani on 07/12/2017.
 */
@EFragment(R.layout.activity_dial_contacts)
public class PhoneContactsFragment extends Fragment {
    @FragmentArg
    Integer sectionNumber;

    Context context;
    Retrofit retrofit;
    User currentUser;

    ArrayList<String> users = new ArrayList<>();
    List<User> sendBirdUsers;

    @ViewById
    RecyclerView contacts;

    @ViewById
    EditText searchbar;

    @Bean
    CallUserAdapter userAdapter;

    public PhoneContactsFragment() {

    }

    @AfterViews
    void afterViews() {
        context = getContext();
        final Activity _this = getActivity();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        contacts.setLayoutManager(linearLayoutManager);
        contacts.setAdapter(userAdapter);

        retrofit = Core.getRetrofit();
        UserService userService = retrofit.create(UserService.class);
        userService.listUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                sendBirdUsers = response.body();
                getPhoneNumbers();
                userAdapter.setItems(users);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPhoneNumbers();
            }
        }
    }

    @TextChange
    void searchbar() {
        if (searchbar.getText().toString().isEmpty()) {
            userAdapter.setItems(users);
        } else {
            Collection<String> filteredUsers = Collections2.filter(users, Predicates.containsPattern(searchbar.getText().toString()));
            userAdapter.setItems(new ArrayList(filteredUsers));
            userAdapter.notifyDataSetChanged();

        }
    }

    void getPhoneNumbers() {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor contacts = getActivity().getContentResolver().query(uri, projection, null, null, null);

        if (contacts != null) {
            int indexPhoneNumber = contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int indexName = contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            contacts.moveToFirst();

            do {
                String phoneNumber = contacts.getString(indexPhoneNumber).replaceAll("\\D+", "");
                String name = contacts.getString(indexName);
                users.add(name + " | " + phoneNumber);
                Collections.sort(users);
            } while (contacts.moveToNext());
        }
    }
}
