package com.studiostorquato.slayapp.views;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallListener;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_call)
public class CallActivity extends SlayAppActivity {
    Context context;

    @ViewById
    TextView status;

    @ViewById
    Button answer;

    @ViewById
    Button hangup;

    @ViewById
    ImageButton mute, unmute, dialer, sound;

    @ViewById
    EditText numbers;

    Call activeCall;

    private final int duration = 4; // seconds
    private final int sampleRate = 8000;
    private final int numSamples = duration * sampleRate;
    private final double sample[] = new double[numSamples];
    private final double freqOfTone = 440; // hz

    InputMethodManager imm;

    AudioManager audioManager;
    AudioTrack audioTrack;

    private final byte generatedSnd[] = new byte[2 * numSamples];

    boolean pause = false;

    @AfterViews
    void afterViews() {
        context = this;
        String phoneNumber = getIntent().getStringExtra("phoneNumber");

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        SinchClient sinchClient = Core.getSinchClient();
        CallClient callClient = sinchClient.getCallClient();
        activeCall = callClient.callPhoneNumber(phoneNumber);
        activeCall.addCallListener(new CallListener() {
            @Override
            public void onCallProgressing(Call call) {
                answer.setVisibility(View.GONE);
                hangup.setVisibility(View.VISIBLE);
                status.setText("Chamando");

                playSound();
            }

            @Override
            public void onCallEstablished(Call call) {
                answer.setVisibility(View.GONE);
                hangup.setVisibility(View.VISIBLE);
                status.setText("Em chamada");
                pauseSound();
            }

            @Override
            public void onCallEnded(Call call) {
                saveToDatabase();
                pauseSound();
                status.setText("Finalizada");
                finish();
            }

            @Override
            public void onShouldSendPushNotification(Call call, List<PushPair> list) {

            }
        });
    }

    @Click
    void hangup() {
        activeCall.hangup();
    }

    void genTone(){
        // fill out the array
        for (int i = 0; i < numSamples; ++i) {
            sample[i] = Math.sin(2 * Math.PI * i / (sampleRate/freqOfTone));
        }

        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalised.
        int idx = 0;
        for (final double dVal : sample) {
            // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
            // in 16 bit wav PCM, first byte is the low order byte
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }

        audioManager.setSpeakerphoneOn(false);

        audioTrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL,
                8000, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT, generatedSnd.length,
                AudioTrack.MODE_STATIC);
        audioTrack.write(generatedSnd, 0, generatedSnd.length/2);
        audioTrack.setNotificationMarkerPosition(generatedSnd.length / 2);
        audioTrack.setPlaybackPositionUpdateListener(new AudioTrack.OnPlaybackPositionUpdateListener() {
            @Override
            public void onMarkerReached(AudioTrack audioTrack) {
                Log.d("reach", "reach");
                playSound();
            }

            @Override
            public void onPeriodicNotification(AudioTrack audioTrack) {

            }
        });
    }

    void playSound() {
        if (!pause) {
            genTone();
            audioTrack.play();
        }
    }

    void pauseSound() {
        pause = true;
        if (audioTrack != null) {
            audioTrack.stop();
        }
    }

    void saveToDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("history");
        reference.child(Core.currentUser().getUserId()).push().setValue(activeCall);
    }

    @Click
    void mute() {
        audioManager.setMicrophoneMute(true);
        mute.setVisibility(View.GONE);
        unmute.setVisibility(View.VISIBLE);
    }

    @Click
    void unmute() {
        audioManager.setMicrophoneMute(false);
        unmute.setVisibility(View.GONE);
        mute.setVisibility(View.VISIBLE);
    }

    @Click
    void dialer() {
        imm.toggleSoftInput(0, 0);
    }

    @Click
    void sound() {
        audioManager.setSpeakerphoneOn(!audioManager.isSpeakerphoneOn());
    }
}
