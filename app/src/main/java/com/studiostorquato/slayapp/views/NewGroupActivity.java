package com.studiostorquato.slayapp.views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@EActivity(R.layout.activity_new_group)
public class NewGroupActivity extends SlayAppActivity {
    @ViewById
    Toolbar toolbar;

    @ViewById
    ImageView profileImage;

    @ViewById
    EditText name;

    String mCurrentPhotoPath;
    String mPhotoUrl;

    int TAKING_PICTURE = 1;
    int FILE_SELECT_CODE = 2;

    @AfterViews
    void afterViews() {
        this.context = getApplicationContext();
        createToolbar(toolbar);
    }

    @Click
    void profileImage() {
        PopupMenu popup = new PopupMenu(context, profileImage);
        popup.getMenuInflater()
                .inflate(R.menu.popup_files, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.camera:
                        takePicture();
                        break;
                    case R.id.gallery:
                        getFromGallery();
                        break;
                }
                return true;
            }
        });
    }

    void getFromGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        startActivityForResult(Intent.createChooser(intent, "Selecione um arquivo"), FILE_SELECT_CODE);
    }

    void takePicture() {
        if (!hasPermission(this, Manifest.permission.CAMERA)) {
            requestPermission(this, Manifest.permission.CAMERA, 0);
        } else {
            final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile = null;

                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.studiostorquato.slayapp.fileprovider",
                            photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, TAKING_PICTURE);
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES.concat("channel_images"));
        File image = File.createTempFile(
                timeStamp,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == TAKING_PICTURE) {
                File file = new File(mCurrentPhotoPath);
                Uri uri = Uri.fromFile(file);
                uploadToFirebase(file, uri);
            } else if (requestCode == FILE_SELECT_CODE) {
                Uri uri = data.getData();
                File file = new File(uri.getPath());
                uploadToFirebase(file, uri);
            }
        }
    }

    private void uploadToFirebase(final File file, Uri uri) {
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageReference = firebaseStorage.getReference().child("channel_images").child(file.getName());

        storageReference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                mPhotoUrl = taskSnapshot.getDownloadUrl().toString();
                Glide.with(context)
                        .load(mPhotoUrl)
                        .apply(RequestOptions.circleCropTransform())
                        .into(profileImage);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture();
            }
        }
    }

    @Click
    void save() {
        if (name.getText().toString().isEmpty()) {
            name.setError("Obrigatório");
        } else {
            ArrayList<User> members = new ArrayList<>();
            members.add(Core.currentUser());
            GroupChannel.createChannel(members, false, name.getText().toString(), "", "", new GroupChannel.GroupChannelCreateHandler() {
                @Override
                public void onResult(GroupChannel groupChannel, SendBirdException e) {
                    if (e == null) {
                        Intent intent = new Intent(context, MembersActivity_.class);
                        intent.putExtra("channel", groupChannel.getUrl());
                        context.startActivity(intent);
                    }
                }
            });
        }
    }
}
