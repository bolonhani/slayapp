package com.studiostorquato.slayapp.views;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBirdException;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_about)
public class AboutActivity extends SlayAppActivity {
    Context context;

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView nickname;

    @ViewById
    ImageView profileImage;

    GroupChannel channel;

    @AfterViews
    void afterViews() {
        context = this;
        createToolbar(toolbar);

        final String channelUrl = getIntent().getStringExtra("channel");
        GroupChannel.getChannel(channelUrl, new GroupChannel.GroupChannelGetHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                channel = groupChannel;
                fill();
            }
        });
    }

    void fill() {
        String name;
        String imageUrl;

        if (channel.getMemberCount() == 2) {
            int position = 0;
            if (channel.getMembers().get(0).getUserId().equals(Core.currentUser().getUserId()))
                position = 1;

            name = channel.getMembers().get(position).getNickname();
            imageUrl = channel.getMembers().get(position).getProfileUrl();

            if (name.isEmpty())
                name = channel.getMembers().get(position).getUserId();
        } else {
            name = channel.getName();
            imageUrl = channel.getCoverUrl();
        }

//        nickname.setText(name);
        Glide.with(this)
                .load(imageUrl)
                .into(profileImage);
    }
}
