package com.studiostorquato.slayapp.views;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import retrofit2.Retrofit;

@EActivity(R.layout.activity_login)
public class LoginActivity extends SlayAppActivity {
    Context context;
    Retrofit retrofit;

    @ViewById
    Toolbar toolbar;

    @ViewById(R.id.constraint_layout)
    ConstraintLayout constraintLayout;

    @ViewById(R.id.phone_number)
    EditText phoneNumber;

    String extractedPhoneNumber;

    @AfterViews
    void afterViews() {
        context = getApplicationContext();
        retrofit = Core.getRetrofit();

        createToolbar(toolbar);

        final MaskedTextChangedListener listener = new MaskedTextChangedListener(
                "+[990] ([990]) [90000] - [0000]",
                true,
                phoneNumber,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean b, String extractedValue) {
                        extractedPhoneNumber = extractedValue;
                    }
                }
        );

        phoneNumber.addTextChangedListener(listener);
    }

    @Click
    void login() {
        SendBird.connect(extractedPhoneNumber, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                if (e != null) {
                    Snackbar.make(constraintLayout, R.string.snackbar_connect_error, Snackbar.LENGTH_LONG).show();
                    e.printStackTrace();
                } else {
                    Core.createUser(user);

                    Intent intent = new Intent(context, DashboardActivity_.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}
