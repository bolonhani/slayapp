package com.studiostorquato.slayapp.views;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;
import com.studiostorquato.slayapp.adapter.MemberAdapter;
import com.studiostorquato.slayapp.sendBirdApi.UserService;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.security.acl.Group;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

@EActivity(R.layout.activity_members)
public class MembersActivity extends SlayAppActivity {
    @ViewById
    Toolbar toolbar;

    @ViewById
    RecyclerView members;

    @Bean
    MemberAdapter memberAdapter;

    public ArrayList<User> selectedUsers = new ArrayList<>();
    ArrayList<User> users = new ArrayList<>();
    List<User> sendBirdUsers;
    Retrofit retrofit;

    GroupChannel channel;

    @AfterViews
    void afterViews() {
        context = getApplicationContext();
        createToolbar(toolbar);
        final Activity _this = this;

        GroupChannel.getChannel(getIntent().getStringExtra("channel"), new GroupChannel.GroupChannelGetHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e == null) {
                    channel = groupChannel;
                }
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        members.setLayoutManager(linearLayoutManager);
        members.setAdapter(memberAdapter);

        retrofit = Core.getRetrofit();
        UserService userService = retrofit.create(UserService.class);
        userService.listUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                sendBirdUsers = response.body();

                if (hasPermission(_this, Manifest.permission.READ_CONTACTS)) {
                    getPhoneNumbers();
                    memberAdapter.setItems(users);
                } else {
                    requestPermission(_this, Manifest.permission.READ_CONTACTS, 1);
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPhoneNumbers();
            }
        }
    }

    void getPhoneNumbers() {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor contacts = getContentResolver().query(uri, projection, null, null, null);

        if (contacts != null) {
            int indexPhoneNumber = contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            contacts.moveToFirst();

            do {
                String phoneNumber = contacts.getString(indexPhoneNumber).replaceAll("\\D+", "");
                User user = getUser(phoneNumber);
                if (user != null) {
                    users.add(user);
                }
            } while (contacts.moveToNext());
        }
    }

    User getUser(String phoneNumber) {
        for (User user : sendBirdUsers) {
            if (user.getUserId().endsWith(phoneNumber)) {
                return user;
            }
        }
        return null;
    }

    @Click
    void save() {
        List<String> userIds = new ArrayList<>();
        for (User user : selectedUsers) {
            userIds.add(user.getUserId());
        }
        channel.inviteWithUserIds(userIds, new GroupChannel.GroupChannelInviteHandler() {
            @Override
            public void onResult(SendBirdException e) {
                if (e == null) {
                    Intent intent = new Intent(context, ChatActivity_.class);
                    intent.putExtra("channel", channel.getUrl());
                    startActivity(intent);
                }
            }
        });
    }
}
