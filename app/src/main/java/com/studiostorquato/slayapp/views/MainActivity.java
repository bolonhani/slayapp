package com.studiostorquato.slayapp.views;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sendbird.android.User;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    Context context;

    @AfterViews
    void afterViews() {
        context = getApplicationContext();
        Core.start(context);

        Core.connectCurrentUser(new Core.UserCallback() {
            @Override
            public void connected(User user) {
                if (user != null) {
                    Intent intent = new Intent(context, DashboardActivity_.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Click
    void register() {
        Intent intent = new Intent(this, LoginActivity_.class);
        startActivity(intent);
    }
}
