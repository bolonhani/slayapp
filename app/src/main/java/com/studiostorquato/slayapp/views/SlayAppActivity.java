package com.studiostorquato.slayapp.views;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.studiostorquato.slayapp.R;

/**
 * Created by felipebolonhani on 01/12/2017.
 */

public class SlayAppActivity extends AppCompatActivity {
    Context context;
    Toolbar toolbar;

    public void createToolbar(Toolbar toolbar) {
        toolbar.setTitle(this.getTitle());

        if (this.getParentActivityIntent() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_iconmonstr_arrow_64);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    boolean hasPermission(Activity activity, String permission) {
        Integer hasPermissions = ContextCompat.checkSelfPermission(activity, permission);
        return hasPermissions == PackageManager.PERMISSION_GRANTED;
    }

    boolean hasPermissions(Activity activity, String[] permissions) {
        for (String permission : permissions) {
            if (!hasPermission(activity, permission)) return false;
        }
        return true;
    }

    void requestPermission(Activity activity, String permission, int requestCode) {
        requestPermissions(activity, new String[]{permission}, requestCode);
    }

    void requestPermissions(Activity activity, String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode);
    }
}
