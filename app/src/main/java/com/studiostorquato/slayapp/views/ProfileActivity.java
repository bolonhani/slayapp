package com.studiostorquato.slayapp.views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_profile)
public class ProfileActivity extends AppCompatActivity {
    Context context;
    User currentUser;

    @ViewById(R.id.profile_image)
    ImageView profileImage;

    @ViewById
    EditText nickname;

    @ViewById
    ConstraintLayout constraint;

    @AfterViews
    void afterViews() {
        context = getApplicationContext();
        currentUser = SendBird.getCurrentUser();

        Glide.with(this)
                .load(currentUser.getProfileUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(profileImage);
        nickname.setText(currentUser.getNickname());
    }

    @Click
    void save() {
        SendBird.updateCurrentUserInfo(nickname.getText().toString(), currentUser.getProfileUrl(), new SendBird.UserInfoUpdateHandler() {
            @Override
            public void onUpdated(SendBirdException e) {
                if (e != null) {
                    Snackbar.make(constraint, R.string.snackbar_update_error, Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(constraint, R.string.snackbar_update_success, Snackbar.LENGTH_LONG).show();
                    currentUser = SendBird.getCurrentUser();
                }
            }
        });
    }
}
