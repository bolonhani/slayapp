package com.studiostorquato.slayapp.views;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sinch.android.rtc.SinchClient;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FocusChange;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.ViewById;

import java.util.HashMap;

/**
 * Created by felipebolonhani on 07/12/2017.
 */
@EFragment(R.layout.tab_dialer)
public class DialerFragment extends Fragment {
    @FragmentArg
    Integer sectionNumber;

    @ViewById(R.id.phone_number)
    EditText phoneNumber;

    String lastNumber;

    SinchClient sinchClient;

    public DialerFragment() {

    }

    @AfterViews
    void afterViews() {
        sinchClient = Core.getSinchClient();

        if (getActivity().getIntent().getStringExtra("phoneNumber") != null) {
            phoneNumber.setText(getActivity().getIntent().getStringExtra("phoneNumber"));
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("history");
        reference.child(Core.currentUser().getUserId()).limitToLast(1).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    String key = ((HashMap) dataSnapshot.getValue()).keySet().toString().replace("[", "").replace("]", "");
                    lastNumber = ((HashMap) ((HashMap) dataSnapshot.getValue()).get(key)).get("remoteUserId").toString().replace("+", "");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Click({R.id.button_1, R.id.button_2, R.id.button_3, R.id.button_4, R.id.button_5, R.id.button_6, R.id.button_7, R.id.button_8, R.id.button_9, R.id.button_asterisk, R.id.button_0, R.id.button_hashtag})
    void addNumber(AppCompatButton clicked) {
        int curPosition = phoneNumber.getSelectionStart();
        String text = phoneNumber.getText().toString();
        String before = text.substring(0, curPosition);
        String after = text.substring(curPosition, text.length());
        String textClicked = clicked.getText().toString();
        phoneNumber.setText(before.concat(textClicked).concat(after));

        phoneNumber.setSelection(++curPosition);
    }

    @Click
    void backspace() {
        int curPos = phoneNumber.getSelectionStart();
        String text = phoneNumber.getText().toString();
        StringBuilder sb = new StringBuilder(text);

        if (!text.isEmpty() && curPos != 0) {
            sb.deleteCharAt(curPos - 1);
            phoneNumber.setText(sb.toString());
            phoneNumber.setSelection(curPos - 1);
        } else {
            phoneNumber.setText(sb.toString());
        }
    }

    @LongClick(R.id.backspace)
    void clear() {
        phoneNumber.setText("");
    }

    @Click
    void contacts() {
        Intent intent = new Intent(getActivity(), DialContactsActivity_.class);
        startActivity(intent);
    }

    @Click
    void call() {
        if (phoneNumber.getText().toString().isEmpty()) {
            phoneNumber.setText(lastNumber);
        } else {
            Intent intent = new Intent(getActivity(), CallActivity_.class);
            intent.putExtra("phoneNumber", "+".concat(phoneNumber.getText().toString()));
            startActivity(intent);
        }
    }

    @FocusChange(R.id.phone_number)
    void phoneNumberFocus(View view, boolean hasFocus) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Click(R.id.phone_number)
    void phoneNumberHide() {
        View view = getActivity().getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
