package com.studiostorquato.slayapp.views;

import android.content.Context;
import android.content.Intent;
import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.PreviousMessageListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.sendbird.android.UserMessage;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;
import com.studiostorquato.slayapp.adapter.MessageAdapter;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.Touch;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@EActivity(R.layout.activity_chat)
public class ChatActivity extends SlayAppActivity {
    Context context;

    int TAKING_PICTURE = 1;
    int FILE_SELECT_CODE = 2;

    @ViewById
    ConstraintLayout constraint;

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView nickname;

    @ViewById
    EditText message;

    @ViewById
    ImageView profileImage;

    @ViewById
    ImageButton sendFiles;

    @ViewById
    ImageButton microphone;

    @ViewById
    ImageButton send;

    @ViewById
    RecyclerView messages;

    @Bean
    MessageAdapter messageAdapter;

    User currentUser;
    GroupChannel channel;
    String mCurrentPhotoPath;
    Uri mPhotoURI;
    MediaRecorder mediaRecorder;
    File mAudio;

    @AfterViews
    void afterViews() {
        context = getApplicationContext();
        createToolbar(toolbar);

        currentUser = Core.currentUser();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        messages.setLayoutManager(linearLayoutManager);
        messages.setAdapter(messageAdapter);

        requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, 1);
        requestPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE, 2);
        requestPermission(this, Manifest.permission.RECORD_AUDIO, 3);

        if (getIntent().getStringExtra("channel") != null) {
            GroupChannel.getChannel(getIntent().getStringExtra("channel"), new GroupChannel.GroupChannelGetHandler() {
                @Override
                public void onResult(GroupChannel groupChannel, SendBirdException e) {
                    if (e != null) {
                        Snackbar.make(constraint, R.string.snackbar_default_error, Snackbar.LENGTH_LONG).show();
                    } else {
                        channel = groupChannel;
                        initializeChat(groupChannel);
                    }
                }
            });
        }
    }

    void initializeChat(GroupChannel groupChannel) {
        String name;
        String imageUrl;

        if (groupChannel.getMemberCount() == 2) {
            int position = 0;
            if (groupChannel.getMembers().get(0).getUserId().equals(Core.currentUser().getUserId()))
                position = 1;

            name = groupChannel.getMembers().get(position).getNickname();
            imageUrl = groupChannel.getMembers().get(position).getProfileUrl();

            if (name.isEmpty())
                name = groupChannel.getMembers().get(position).getUserId();
        } else {
            name = groupChannel.getName();
            imageUrl = groupChannel.getCoverUrl();
        }

        nickname.setText(name);
        Glide.with(context)
                .load(imageUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(profileImage);

        getMessages();
    }

    void getMessages() {
        PreviousMessageListQuery previousMessageListQuery = channel.createPreviousMessageListQuery();
        previousMessageListQuery.load(30, false, new PreviousMessageListQuery.MessageListQueryResult() {
            @Override
            public void onResult(List<BaseMessage> list, SendBirdException e) {
                if (e == null) {
                    messageAdapter.setItems(list);
                    messages.scrollToPosition(list.size() - 1);
                }
            }
        });

        SendBird.addChannelHandler(channel.getUrl(), new SendBird.ChannelHandler() {
            @Override
            public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                getMessages();
            }
        });
    }

    @AfterTextChange(R.id.message)
    void messageChanged(TextView message) {
        if (message.getText().toString().isEmpty()) {
            microphone.setVisibility(View.VISIBLE);
            send.setVisibility(View.GONE);
            send.setClickable(false);
        } else {
            microphone.setVisibility(View.GONE);
            send.setVisibility(View.VISIBLE);
            send.setClickable(true);
        }
    }

    @Click
    void send() {
        send.setClickable(false);
        channel.sendUserMessage(message.getText().toString(), new BaseChannel.SendUserMessageHandler() {
            @Override
            public void onSent(UserMessage userMessage, SendBirdException e) {
                if (e != null) {
                    Snackbar.make(constraint, R.string.snackbar_default_error, Snackbar.LENGTH_LONG).show();
                }

                getMessages();
                message.setText("");
                send.setClickable(true);
            }
        });
    }

    @Click
    void sendFiles() {
        PopupMenu popup = new PopupMenu(context, sendFiles);
        popup.getMenuInflater()
                .inflate(R.menu.popup_files, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.camera:
                        takePicture();
                        break;
                    case R.id.gallery:
                        getFromGallery();
                        break;
                }
                return true;
            }
        });
    }

    void getFromGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        startActivityForResult(Intent.createChooser(intent, "Selecione um arquivo"), FILE_SELECT_CODE);
    }

    void takePicture() {
        if (!hasPermission(this, Manifest.permission.CAMERA)) {
            requestPermission(this, Manifest.permission.CAMERA, 0);
        } else {
            final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile = null;

                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.studiostorquato.slayapp.fileprovider",
                            photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, TAKING_PICTURE);
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES.concat("/" + channel.getUrl()));
        File image = File.createTempFile(
                timeStamp,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == TAKING_PICTURE) {
                File file = new File(mCurrentPhotoPath);
                Uri uri = Uri.fromFile(file);
                uploadToFirebase(file, uri);
            } else if (requestCode == FILE_SELECT_CODE) {
                Uri uri = data.getData();
                File file = new File(uri.getPath());
                uploadToFirebase(file, uri);
            }
        }
    }

    private void uploadToFirebase(final File file, Uri uri) {
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageReference = firebaseStorage.getReference(channel.getUrl()).child(file.getName());

        storageReference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                String url = taskSnapshot.getDownloadUrl().toString();
                int size = (int) taskSnapshot.getBytesTransferred();
                String mime = taskSnapshot.getMetadata().getContentType();
                channel.sendFileMessage(url, file.getName(), mime, size, "", new BaseChannel.SendFileMessageHandler() {
                    @Override
                    public void onSent(FileMessage fileMessage, SendBirdException e) {
                        getMessages();
                    }
                });
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture();
            }
        }
    }

    @Click({R.id.profile_image, R.id.nickname})
    void about() {
        Intent intent = new Intent(context, AboutActivity_.class);
        intent.putExtra("channel", channel.getUrl());
        startActivity(intent);
    }

    @Touch
    void microphone(View view, MotionEvent motionEvent) {
        int screenSize = getWindowManager().getDefaultDisplay().getWidth();
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            message.setEnabled(false);
            message.setHint("< Deslize para cancelar");
            microphone.setBackground(getResources().getDrawable(R.drawable.microphone_active));

            startRecording();
        } else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
            if (motionEvent.getRawX() < (screenSize / 3)) {
                resetMicrophone();
                if (mediaRecorder != null) {
                    mediaRecorder.stop();
                    mediaRecorder.release();
                    mediaRecorder = null;
                }
            }
        } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            resetMicrophone();
            mediaRecorder = null;

            if (motionEvent.getRawX() > (screenSize / 3)) {
                this.uploadToFirebase(mAudio, Uri.fromFile(mAudio));
            }
        }
    }

    void resetMicrophone() {
        message.setEnabled(true);
        message.setHint(R.string.type_a_message);
        microphone.setBackground(null);
    }

    void startRecording() {
        try {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File storage = getExternalFilesDir(Environment.DIRECTORY_MUSIC.concat("/" + channel.getUrl()));

            mAudio = File.createTempFile(
                    timeStamp,
                    ".3gpp",
                    storage
            );

            mediaRecorder.setOutputFile(mAudio.getPath());
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
