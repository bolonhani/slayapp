package com.studiostorquato.slayapp.views;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sinch.android.rtc.calling.Call;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;
import com.studiostorquato.slayapp.adapter.HistoryAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by felipebolonhani on 07/12/2017.
 */
@EFragment(R.layout.tab_history)
public class HistoryFragment extends Fragment {
    @FragmentArg
    Integer sectionNumber;

    @ViewById
    RecyclerView history;

    @Bean
    HistoryAdapter adapter;

    HashMap<String, Object> response;

    @AfterViews
    void afterViews() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        history.setLayoutManager(linearLayoutManager);
        history.setAdapter(adapter);

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference reference = firebaseDatabase.getReference("history");
        reference.child(Core.currentUser().getUserId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    response = (HashMap<String, Object>) dataSnapshot.getValue();
                    List<Object> items = Arrays.asList(response.values().toArray());
                    Collections.sort(items, new Comparator<Object>() {
                        @Override
                        public int compare(Object first, Object second) {
                            long firstTime = (Long) ((HashMap) ((HashMap) first).get("details")).get("startedTime");
                            long secondTime = (Long) ((HashMap) ((HashMap) second).get("details")).get("startedTime");

                            if (firstTime < secondTime) return 1;
                            if (firstTime > secondTime) return -1;
                            return 0;
                        }
                    });

                    adapter.setItems(items);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
