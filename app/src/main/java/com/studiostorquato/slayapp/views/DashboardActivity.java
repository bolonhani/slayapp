package com.studiostorquato.slayapp.views;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.GroupChannelListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;
import com.studiostorquato.slayapp.adapter.ChatAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_dashboard)
public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private String CHANNEL_HANDLER = "slay_app_channel_handler";

    Context context;
    User currentUser;

    @ViewById
    Toolbar toolbar;

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawer;

    @ViewById(R.id.nav_view)
    NavigationView navigationView;

    @ViewById
    RecyclerView channels;

    @ViewById
    ImageButton popover;

    @Bean
    ChatAdapter chatAdapter;

    @AfterViews
    void afterViews() {
        context = getApplicationContext();
        currentUser = Core.currentUser();

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toogle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toogle);
        toogle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        channels.setLayoutManager(linearLayoutManager);
        channels.setAdapter(chatAdapter);

        getChats();

        SendBird.addChannelHandler("general", new SendBird.ChannelHandler() {
            @Override
            public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                getChats();
            }
        });
    }

    void getChats() {
        GroupChannelListQuery groupChannelListQuery = GroupChannel.createMyGroupChannelListQuery();
        groupChannelListQuery.next(new GroupChannelListQuery.GroupChannelListQueryResultHandler() {
            @Override
            public void onResult(List<GroupChannel> list, SendBirdException e) {
                if (e != null) {
                    Snackbar.make(drawer, e.getMessage(), Snackbar.LENGTH_LONG).show();
                } else {
                    chatAdapter.setItems(list);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ImageView profileImage = navigationView.findViewById(R.id.profile_image);
        TextView nickname = navigationView.findViewById(R.id.nickname);
        TextView userId = navigationView.findViewById(R.id.userId);

        nickname.setText(currentUser.getNickname());
        userId.setText(currentUser.getUserId());
        Glide.with(this)
                .load(currentUser.getProfileUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(profileImage);

        return super.onCreateOptionsMenu(menu);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.profile) {
            Intent intent = new Intent(this, ProfileActivity_.class);
            startActivity(intent);
        } else if (id == R.id.settings) {

        } else if (id == R.id.invite_friends) {

        } else if (id == R.id.contacts) {
            Intent intent = new Intent(this, ContactsActivity_.class);
            startActivity(intent);
        } else if (id == R.id.logout) {
            Core.logout(new SendBird.DisconnectHandler() {
                @Override
                public void onDisconnected() {
                    Intent intent = new Intent(context, MainActivity_.class);
                    startActivity(intent);
                    finish();
                }
            });
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Click
    void popover() {
        PopupMenu menu = new PopupMenu(context, popover);
        menu.getMenuInflater().inflate(R.menu.dashboard, menu.getMenu());
        menu.show();

        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.new_group:
                        Intent intent = new Intent(context, NewGroupActivity_.class);
                        startActivity(intent);
                }
                return false;
            }
        });
    }

    @Click
    void dialer() {
        Intent intent = new Intent(context, DialerActivity_.class);
        startActivity(intent);
    }
}
