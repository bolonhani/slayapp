package com.studiostorquato.slayapp.views;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.sendbird.android.User;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;
import com.studiostorquato.slayapp.adapter.UserAdapter;
import com.studiostorquato.slayapp.sendBirdApi.UserService;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

@EActivity(R.layout.activity_contacts)
public class ContactsActivity extends SlayAppActivity {
    Context context;
    Retrofit retrofit;
    User currentUser;

    ArrayList<User> users = new ArrayList<User>();
    List<User> sendBirdUsers;

    @ViewById
    Toolbar toolbar;

    @ViewById
    RecyclerView contactList;

    @Bean
    UserAdapter userAdapter;

    @AfterViews
    void afterViews() {
        context = getApplicationContext();
        final Activity _this = this;
        createToolbar(toolbar);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        contactList.setLayoutManager(linearLayoutManager);
        contactList.setAdapter(userAdapter);

        retrofit = Core.getRetrofit();
        UserService userService = retrofit.create(UserService.class);
        userService.listUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                sendBirdUsers = response.body();

                if (hasPermission(_this, Manifest.permission.READ_CONTACTS)) {
                    getPhoneNumbers();
                    userAdapter.setItems(users);
                } else {
                    requestPermission(_this, Manifest.permission.READ_CONTACTS, 1);
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPhoneNumbers();
            }
        }
    }

    void getPhoneNumbers() {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor contacts = getContentResolver().query(uri, projection, null, null, null);

        if (contacts != null) {
            int indexPhoneNumber = contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            contacts.moveToFirst();

            do {
                String phoneNumber = contacts.getString(indexPhoneNumber).replaceAll("\\D+", "");
                User user = getUser(phoneNumber);
                if (user != null) {
                    users.add(user);
                }
            } while (contacts.moveToNext());
        }
    }

    User getUser(String phoneNumber) {
        for (User user : sendBirdUsers) {
            if (user.getUserId().endsWith(phoneNumber)) {
                Log.d("Got", phoneNumber);
                return user;
            }
        }
        return null;
    }
}
