package com.studiostorquato.slayapp.component;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sendbird.android.User;
import com.studiostorquato.slayapp.R;
import com.studiostorquato.slayapp.views.MembersActivity;
import com.studiostorquato.slayapp.views.MembersActivity_;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Created by felipebolonhani on 09/12/2017.
 */
@EViewGroup(R.layout.component_member)
public class MemberComponent extends LinearLayout {
    Context context;

    @ViewById
    ImageView profileImage;

    @ViewById
    TextView name;

    @ViewById
    CheckBox isMember;

    ArrayList<User> selectedUsers;

    public MemberComponent(Context context) {
        super(context);
        this.context = context;
        this.selectedUsers = new ArrayList<>();
    }

    public void bind(final User user) {
        String username = user.getNickname().isEmpty() ? user.getUserId() : user.getNickname();
        name.setText(username);
        Glide.with(this)
                .load(user.getProfileUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(profileImage);

        OnClickListener listener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isMember.isChecked())
                    ((MembersActivity) context).selectedUsers.remove(user);
                else
                    ((MembersActivity) context).selectedUsers.add(user);
                isMember.setChecked(!isMember.isChecked());
            }
        };

        setOnClickListener(listener);
//        isMember.setOnClickListener(listener);
    }
}
