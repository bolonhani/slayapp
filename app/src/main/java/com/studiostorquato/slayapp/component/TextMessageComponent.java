package com.studiostorquato.slayapp.component;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.UserMessage;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.checkerframework.checker.linear.qual.Linear;

import java.io.File;
import java.io.IOException;

/**
 * Created by felipebolonhani on 02/12/2017.
 */
@EViewGroup(R.layout.component_message)
public class TextMessageComponent extends LinearLayout {
    Context context;

    @ViewById
    RelativeLayout constraint;

    @ViewById
    LinearLayout wrapper;

    @ViewById
    TextView messageTextView;

    @ViewById
    ImageView image;

    @ViewById
    ImageButton playButton;

    public TextMessageComponent(Context context) {
        super(context);
        this.context = context;
    }

    private boolean mine(BaseMessage message) {
        if (message instanceof UserMessage) {
            return ((UserMessage) message).getSender().getUserId().equals(Core.currentUser().getUserId());
        } else if (message instanceof FileMessage) {
            return ((FileMessage) message).getSender().getUserId().equals(Core.currentUser().getUserId());
        }

        return false;
    }

    private boolean createNewFile(File file) {
        try {
            return file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            file.getParentFile().mkdir();
            return createNewFile(file);
        }
    }

    private Uri getAudioPathIfDownloaded(FileMessage message, OnSuccessListener<FileDownloadTask.TaskSnapshot> callback) {
        File directory = context.getExternalFilesDir(Environment.DIRECTORY_MUSIC.concat("/" + message.getChannelUrl()));
        String[] paths = Uri.decode(message.getUrl()).split("/");
        String filename = paths[paths.length - 1].split("\\?")[0];
        String dir = Uri.decode(directory.getPath().concat("/" + filename));
        File file = new File(dir);

        if (!file.exists()) {
            if(!createNewFile(file)) {
                Log.d("Erro", "erro");
            }

            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
            StorageReference reference = firebaseStorage.getReferenceFromUrl(message.getUrl());
            reference.getFile(file).addOnSuccessListener(callback);
        }

        return FileProvider.getUriForFile(context,
                "com.studiostorquato.slayapp.fileprovider",
                file);
    }

    private Uri getFilePathIfDownloaded(FileMessage message, OnSuccessListener<FileDownloadTask.TaskSnapshot> callback) {
        File directory = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES.concat("/" + message.getChannelUrl()));
        String[] paths = Uri.decode(message.getUrl()).split("/");
        String filename = paths[paths.length - 1].split("\\?")[0];
        String dir = Uri.decode(directory.getPath().concat("/" + filename));
        File file = new File(dir);

        if (!file.exists()) {
            if(!createNewFile(file)) {
                Log.d("Erro", "erro");
            }

            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
            StorageReference reference = firebaseStorage.getReferenceFromUrl(message.getUrl());
            reference.getFile(file).addOnSuccessListener(callback);
        }

        return FileProvider.getUriForFile(context,
                "com.studiostorquato.slayapp.fileprovider",
                file);
//        return Uri.fromFile(file);
    }

    private void openFile(Uri uri, String mimeType) {
        if (mimeType == null) {
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            mimeType = mimeTypeMap.getMimeTypeFromExtension(uri.getLastPathSegment());
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, mimeType);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.startActivity(intent);
    }

    public void bind(final BaseMessage message) {
        constraint.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        if (mine(message)) {
            wrapper.setBackground(getResources().getDrawable(R.drawable.bubble_me));
            messageTextView.setTextColor(getResources().getColor(R.color.colorDark));
            constraint.setGravity(Gravity.END);
        }

        final OnSuccessListener<FileDownloadTask.TaskSnapshot> callback = new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d("ok", "ok");
            }
        };

        if (message instanceof UserMessage) {
            createTextView((UserMessage) message);
        } else if (message instanceof FileMessage) {
            createFileView((FileMessage) message);
        }
    }

    void createTextView(UserMessage message) {
        messageTextView.setText(message.getMessage());
        messageTextView.setVisibility(VISIBLE);
    }

    void createFileView(final FileMessage message) {
//        image.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                openFile(getFilePathIfDownloaded(message, callback), null);
//            }
//        });

        if (message.getType().contains("image")) {
            Log.d("Type image", String.valueOf(message.getMessageId()));
            Glide.with(context)
                    .load(message.getUrl())
                    .into(image);
            image.setVisibility(VISIBLE);
        } else if (message.getType().contains("3gpp")) {
            Log.d("Type audio", String.valueOf(message.getMessageId()));
            try {
                playButton.setVisibility(VISIBLE);
                MediaPlayer mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(message.getUrl());
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (message.getType().contains("video")) {
            Log.d("Type video", String.valueOf(message.getMessageId()));
            Glide.with(context)
                    .load(message.getUrl())
                    .into(image);
            image.setVisibility(VISIBLE);
        }
    }
}
