package com.studiostorquato.slayapp.component;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextClock;
import android.widget.TextView;

import com.sinch.android.rtc.calling.Call;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by felipebolonhani on 07/12/2017.
 */
@EViewGroup(R.layout.component_history)
public class HistoryComponent extends LinearLayout {
    Context context;

    @ViewById
    TextView number;

    @ViewById
    TextView time;

    public HistoryComponent(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(HashMap call) {
        number.setText(call.get("remoteUserId").toString());
        long timestamp = (Long) ((HashMap) call.get("details")).get("startedTime");
        Date date = new Date(timestamp);
        time.setVisibility(GONE);
    }
}
