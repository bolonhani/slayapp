package com.studiostorquato.slayapp.component;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.User;
import com.sendbird.android.UserMessage;
import com.studiostorquato.slayapp.views.ChatActivity_;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by felipebolonhani on 01/12/2017.
 */
@EViewGroup(R.layout.component_chat)
public class ChatComponent extends LinearLayout {
    Context context;

    @ViewById
    ConstraintLayout constraint;

    @ViewById(R.id.profile_image)
    ImageView profileImage;

    @ViewById
    TextView nickname;

    @ViewById
    TextView lastMessage;

    public ChatComponent(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(final GroupChannel groupChannel) {
        String groupChannelNickname;
        String groupChannelImageUrl;
        if (groupChannel.getMemberCount() == 2) {
            int position = 0;
            if (groupChannel.getMembers().get(0).getUserId().equals(Core.currentUser().getUserId()))
                position = 1;

            groupChannelNickname = groupChannel.getMembers().get(position).getNickname();
            groupChannelImageUrl = groupChannel.getMembers().get(position).getProfileUrl();

            if (groupChannelNickname.isEmpty())
                groupChannelNickname = groupChannel.getMembers().get(position).getUserId();
        } else {
            groupChannelNickname = groupChannel.getName();
            groupChannelImageUrl = groupChannel.getCoverUrl();
        }

        nickname.setText(groupChannelNickname);
        if (groupChannel.getLastMessage() instanceof UserMessage) {
            lastMessage.setText(((UserMessage) groupChannel.getLastMessage()).getMessage());
        } else if (groupChannel.getLastMessage() instanceof FileMessage) {
            lastMessage.setText(((FileMessage) groupChannel.getLastMessage()).getType());
        }
        Glide.with(this)
                .load(groupChannelImageUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(profileImage);

        constraint.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatActivity_.class);
                intent.putExtra("channel", groupChannel.getUrl());
                context.startActivity(intent);
            }
        });
    }
}
