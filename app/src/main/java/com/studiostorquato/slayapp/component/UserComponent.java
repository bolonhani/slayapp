package com.studiostorquato.slayapp.component;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.Member;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.views.ChatActivity_;
import com.studiostorquato.slayapp.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by felipebolonhani on 01/12/2017.
 */
@EViewGroup(R.layout.component_user)
public class UserComponent extends LinearLayout {
    Context context;

    @ViewById
    ConstraintLayout contact;

    @ViewById(R.id.profile_image)
    ImageView profileImage;

    @ViewById
    TextView nickname;

    public UserComponent(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(final User user) {
        if (user.getNickname().isEmpty())
            nickname.setText(user.getUserId());
        else
            nickname.setText(user.getNickname());
        Glide.with(this)
                .load(user.getProfileUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(profileImage);
        contact.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                createChannel(user);
            }
        });
    }

    private void createChannel(User user) {
        List<User> members = new ArrayList<>();
        members.add(Core.currentUser());
        members.add(user);

        GroupChannel.createChannel(members, true, new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {

                } else {
                    Intent intent = new Intent(context, ChatActivity_.class);
                    intent.putExtra("channel", groupChannel.getUrl());
                    context.startActivity(intent);
                }
            }
        });
    }
}
