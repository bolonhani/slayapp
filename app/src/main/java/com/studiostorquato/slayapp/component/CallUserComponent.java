package com.studiostorquato.slayapp.component;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.Member;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.studiostorquato.slayapp.Core;
import com.studiostorquato.slayapp.views.CallActivity_;
import com.studiostorquato.slayapp.views.ChatActivity_;
import com.studiostorquato.slayapp.R;
import com.studiostorquato.slayapp.views.DialerActivity_;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by felipebolonhani on 01/12/2017.
 */
@EViewGroup(R.layout.component_call_user)
public class CallUserComponent extends LinearLayout {
    Context context;

    @ViewById
    ConstraintLayout contact;

    @ViewById
    ImageView profileImage;

    @ViewById
    TextView nickname;

    public CallUserComponent(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(final String phoneNumber) {
        nickname.setText(phoneNumber);
        contact.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                call(phoneNumber);
            }
        });
    }

    void call(String phoneNumber) {
        String pn = phoneNumber.split("\\|")[1];
        Intent intent = new Intent(context, DialerActivity_.class);
        intent.putExtra("phoneNumber", pn);
        context.startActivity(intent);
    }
}
