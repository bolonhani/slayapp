package com.studiostorquato.slayapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.studiostorquato.slayapp.interceptor.MyInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by felipebolonhani on 30/11/2017.
 */

public class Core {
    private static Context context;
    private static boolean coreStarted = false;
    private static Retrofit retrofit;
    private static Gson gson;
    private static OkHttpClient httpClient;
    private static LocationManager mLocationManager;
    private static FirebaseApp firebase;
    private static SinchClient sinchClient;

    public static final String BASE_URL = "https://api.sendbird.com/v3/";

    public static void start(Context context) {

        if (!coreStarted) {
            synchronized (Core.class) {
                if (!coreStarted) {
                    coreStarted = true;
                    Core.context = context;

                    loadRetrofit();
                    firebase = FirebaseApp.initializeApp(context);
                    SendBird.init("1FA9F36C-3CD9-4DD4-87E9-D43DC431B853", context);
                }
            }
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static void loadRetrofit() {
        interceptors();

        gson = new GsonBuilder()
                .setDateFormat("dd-MM-yyyy'T'HH:mm:ssZ")
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build();
    }

    public static void interceptors() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient = new OkHttpClient.Builder()
                .addInterceptor(new MyInterceptor())
                .addInterceptor(loggingInterceptor)
                .build();
    }

    public interface UserCallback {
        void connected(User user);
    }

    public static void connectCurrentUser(final UserCallback userCallback) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (sharedPreferences.contains("id")) {
            SendBird.connect(sharedPreferences.getString("id", null), new SendBird.ConnectHandler() {
                @Override
                public void onConnected(User user, SendBirdException e) {
                    userCallback.connected(user);
                    startSinch(user.getUserId());
                }
            });
        }
    }

    public static User currentUser() {
        return SendBird.getCurrentUser();
    }

    public static SinchClient getSinchClient() {
        if (sinchClient == null) {
            Core.startSinch(Core.currentUser().getUserId());
            return getSinchClient();
        }
        return sinchClient;
    }

    public static void createUser(User user) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("id", user.getUserId());

        editor.apply();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        database.getReference("contacts").child(user.getUserId()).child("created").setValue(true);
    }

    public static void startSinch(String userId) {
        sinchClient = Sinch.getSinchClientBuilder().context(context)
                .applicationKey("d0e59f78-b477-4b34-bbf3-f4a7abfa2087")
                .applicationSecret("4sfytQ1x2E6MwWNUxpM98Q==")
                .environmentHost("sandbox.sinch.com")
                .userId(userId)
                .build();

        sinchClient.setSupportCalling(true);

        sinchClient.addSinchClientListener(new SinchClientListener() {
            @Override
            public void onClientStarted(SinchClient sinchClient) {
                Log.d("Sinch", "Client started");
            }

            @Override
            public void onClientStopped(SinchClient sinchClient) {
                Log.d("Sinch", "Client stopped");
            }

            @Override
            public void onClientFailed(SinchClient sinchClient, SinchError sinchError) {
                Log.d("Sinch", "Client failed");
            }

            @Override
            public void onRegistrationCredentialsRequired(SinchClient sinchClient, ClientRegistration clientRegistration) {
                Log.d("Sinch", "onRegistrationCredentialsRequired");
            }

            @Override
            public void onLogMessage(int i, String s, String s1) {
                Log.d("Sinch", "onLogMessage");
            }
        });
        sinchClient.start();
    }

    public static void logout(SendBird.DisconnectHandler disconnectHandler) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().clear().apply();
        SendBird.disconnect(disconnectHandler);
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

}
