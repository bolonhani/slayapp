package com.studiostorquato.slayapp.utils;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.webkit.MimeTypeMap;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.DecimalFormat;
import java.util.Hashtable;

/**
 * Created by felipebolonhani on 03/12/2017.
 * DateUtils related to file handling (for sending / downloading file messages).
 */

public class FileUtils {
    Context context;
    Uri uri;

    public FileUtils(Context context, Uri uri) {
        this.context = context;
        this.uri = uri;
    }

    public String getMime() {
        ContentResolver cR = context.getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String type = mime.getExtensionFromMimeType(cR.getType(uri));
        return type;
    }

    private Cursor getCursor() {
        Cursor cursor = this.context.getContentResolver().query(this.uri, null, null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public String getName() {
        Cursor cursor = getCursor();
        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        return cursor.getString(nameIndex);
    }

    public int getSize() {
        Cursor cursor = getCursor();
        int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
        return (int) cursor.getLong(sizeIndex);
    }

    public File getFile() {
        return new File(this.uri.getPath());
    }
}