package com.studiostorquato.slayapp;

import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.SendBird;
import com.sinch.android.rtc.SinchClient;

/**
 * Created by felipebolonhani on 01/12/2017.
 */

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
