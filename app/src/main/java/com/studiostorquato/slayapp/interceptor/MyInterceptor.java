package com.studiostorquato.slayapp.interceptor;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.studiostorquato.slayapp.Core;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by felipebolonhani on 30/11/2017.
 */

public class MyInterceptor implements Interceptor {
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request original = chain.request();
        Request request;

        request = original.newBuilder()
                .addHeader("Api-Token", "a112159d531f3fb86e6a186fec754da3f9dd6b98")
                .build();

        Response response = chain.proceed(request);
        String newBody = treatBody(response.body().string());
        ResponseBody body = ResponseBody.create(response.body().contentType(), newBody);
        Response newResponse = response.newBuilder().body(body).build();

        return newResponse;
    }

    String treatBody(String body) {
        body = body
//                .replaceAll("has_ever_logged_in", "")
                .replaceAll("user_id", "mUserId")
//                .replaceAll("is_active", "")
//                .replaceAll("is_online", "")
                .replaceAll("last_seen_at", "mLastSeenAt")
                .replaceAll("nickname", "mNickname")
                .replaceAll("profile_url", "mProfileUrl")
                .replaceAll("metadata", "mMetaData");
        JsonObject json = (JsonObject) new JsonParser().parse(body);
        return json.get("users").toString();
    }
}
