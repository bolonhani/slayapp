package com.studiostorquato.slayapp.sendBirdApi;

import com.google.gson.JsonElement;
import com.sendbird.android.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by felipebolonhani on 30/11/2017.
 */

public interface UserService {
    @POST("users")
    Call<JsonElement> createUser(@Body JsonElement user);

    @GET("users")
    Call<List<User>> listUsers();

    @GET("users/{id}")
    Call<User> getUser(@Path("id") Integer id);
}
